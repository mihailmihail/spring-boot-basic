package com.example.demo.dao;

import com.example.demo.model.Person;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.jdbc.Sql;

import javax.sql.DataSource;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@JdbcTest
@Sql({"V1__PersonTable"})
public class PersonDataAccessServiceTest {

    private PersonDataAccessService dataAccessService;
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        dataSource = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .addScript("classpath:db/migration/V1__PersonTable.sql")
                .build();
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void crudOperations() {
        dataAccessService = new PersonDataAccessService(jdbcTemplate);

        UUID idOne = UUID.randomUUID();
        Person personOne = new Person(idOne, "Harambe");

        UUID idTwo = UUID.randomUUID();
        Person personTwo = new Person(idTwo, "Gorilla");

        // Insert two persons
        dataAccessService.insertPerson(idOne, personOne);
        dataAccessService.insertPerson(idTwo, personTwo);

        // Can retrieve persons one by one
        assertThat(dataAccessService.selectPersonById(idOne))
                .isPresent()
                .hasValueSatisfying(person -> assertThat(person).isEqualToComparingFieldByField(personOne));

        assertThat(dataAccessService.selectPersonById(idTwo))
                .isPresent()
                .hasValueSatisfying(person -> assertThat(person).isEqualToComparingFieldByField(personTwo));

        // Can retrieve all persons
        List<Person> allPeople = dataAccessService.selectAllPeople();
        assertThat(allPeople)
                .hasSize(2)
                .usingFieldByFieldElementComparator()
                .containsExactlyInAnyOrder(personOne, personTwo);

        // Can update person by ID
        Person personOneUpdate = new Person(idOne, "Tarzan");
        assertThat(dataAccessService.updatePersonById(idOne, personOneUpdate)).isEqualTo(1);
        assertThat(dataAccessService.selectPersonById(idOne))
                .isPresent()
                .hasValueSatisfying(person -> assertThat(person).isEqualToComparingFieldByField(personOneUpdate));
        // Will return 0 if no id found
        UUID fakeId = UUID.randomUUID();
        assertThat(dataAccessService.updatePersonById(fakeId, personOneUpdate)).isEqualTo(0);

        // Can delete user by ID
        assertThat(dataAccessService.deletePersonById(idOne)).isEqualTo(1);
        // Only one person remains in database
        assertThat(dataAccessService.selectAllPeople())
                .hasSize(1)
                .usingFieldByFieldElementComparator()
                .containsExactly(personTwo);
        try {
            dataAccessService.selectPersonById(idOne);
            fail("Result size was not 0");
        } catch (EmptyResultDataAccessException ignored) {
        }
        // Will delete 0 if no id is found
        assertThat(dataAccessService.deletePersonById(fakeId)).isEqualTo(0);
    }
}