package com.example.demo.datasource;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
public class testDbDataSource {

    @Bean
    @ConfigurationProperties("test.datasource")
    public HikariDataSource hikariDataSource () {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }
}
