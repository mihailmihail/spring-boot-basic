# `Java Spring Boot Demo`
Restful webserver with dependency injection data access layer.

**Please provide datasource configuration to:**
```
**/src/main/resources/application.yml
```